import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Provider, connect } from 'react-redux';
import Routes from './src/routes';
import Store from './src/redux/store';
import SplashScreen from 'react-native-splash-screen'

export default class App extends Component {
  componentDidMount() {
      SplashScreen.hide();
  }
  render() {
    return (
      <Provider store={Store}>
        <Routes />
      </Provider>
    );
  }
}
