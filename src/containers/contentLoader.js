import React from 'react';
import ContentLoader from 'react-native-content-loader';
import {View} from 'react-native';


export const Contentloader = () =>{
    const loaderData =[] 
    for (let index = 0; index < 4; index++) {
         loaderData.push(<View key={index} style={{marginTop: 10}}>
         <ContentLoader 
            rtl
            height={250}
            width={400}
            speed={1}
            primaryColor="#ededf0"
            secondaryColor="#e3e2e4">
            <rect x="11" y="-77" rx="0" ry="0" width="520" height="216" /> 
            <rect x="190" y="142" rx="0" ry="0" width="0" height="0" /> 
            <rect x="30" y="162" rx="0" ry="0" width="335" height="13" />
        </ContentLoader>
        </View>)
    }
    return loaderData;
};