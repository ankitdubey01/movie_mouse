import React, { Component } from "react";
import {Text, View, StyleSheet, Image, ImageBackground, TouchableOpacity,} from 'react-native';
import { Card } from "native-base";

class Poster extends Component {
    render() {
        const {item, navigation} = this.props;
        return(
                <TouchableOpacity 
                    onPress={() => {
                    navigation.navigate('showtime', {
                        film_id: item.film_id
                    })
                }}>
                    <Card>
                        <View>
                            {
                                item.images.poster[Object.keys(item.images.poster)[0]] ? 
                                <ImageBackground 
                                source={{ uri: item.images.poster[Object.keys(item.images.poster)[0]].medium.film_image}} 
                                blurRadius={1} 
                                style={styles.backgroundImage} >
                                    <Image 
                                        source={{ uri: item.images.poster[Object.keys(item.images.poster)[0]].medium.film_image}}
                                        style={styles.imageStyle} 
                                    />
                                </ImageBackground>
                                :
                                <Image 
                                    source={require('../assets/dummy.png')}
                                    style={styles.imageStyle} 
                                />  
                            }
                        </View>

                        <View style={styles.CardView}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-start', marginBottom: 5}}>
                                <Text style={styles.textStyle} >{item.film_name}</Text>
                                <Text style={{color: '#ef0259',fontSize: 12,fontWeight: 'bold', marginLeft: 10}}>({item.age_rating[0].rating})</Text>
                            </View>
                        </View>
                        
                    </Card>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    backgroundImage: {
        height: 150, 
        width: '100%', 
        resizeMode: 'stretch',
        borderRadius: 10
    },
    imageStyle: {
        height: 150, 
        width: '100%',
        flex: 1,
        resizeMode: 'contain',
        borderRadius: 5
    },
    CardView: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 5
    },
    textStyle: {
        fontSize: 12,
    },
})

export default Poster