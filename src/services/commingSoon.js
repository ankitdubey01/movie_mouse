import {env} from '../helpers/env';

export const CommingSoon = (args) => {
    // let Url = `${env.apiUrl}/filmsComingSoon/?n=15`
    let Url = `${env.apiUrl}/films-coming-soon`
    const options = {
        method: 'GET',
        headers: {
            'client': env.username,
            'x-api-key': env.api_key,
            'authorization': env.authorization,
            'api-version': env.api_version,
            'territory': env.territory,
            'device-datetime': args.isoDate
        }
    }
    return fetch(Url, options)
            .then(res => res.json())
            .then(res => {
                console.log('now showing response', res);
                return res.data
            })
}