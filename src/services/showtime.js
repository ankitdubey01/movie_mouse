import {env} from '../helpers/env';

export const showtime = (args) => {
    // let Url = `${env.apiUrl}/filmShowTimes/?film_id=${args.filmId}&date=${args.localdate}`
    let Url = `${env.apiUrl}/film-show-time`

    const options = {
        method: 'GET',
        headers: {
            'client': env.username,
            'x-api-key': env.api_key,
            'authorization': env.authorization,
            'api-version': env.api_version,
            'territory': env.territory,
            'device-datetime': args.isoDate,
            'geolocation': `${args.lat};${args.lon}`
        }
    }
    return fetch(Url, options)
            .then(res => res.json())
            .then(res => {
                console.log('show time response', res);
                return res.data
            })
}