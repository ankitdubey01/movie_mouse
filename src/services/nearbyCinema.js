import {env} from '../helpers/env';

export const nearbyCinema = (args) => {
    // let Url = `${env.apiUrl}/cinemasNearby/?n=25`
    let Url = `${env.apiUrl}/cinema-near-by`
    const options = {
        method: 'GET',
        headers: {
            'client': env.username,
            'x-api-key': env.api_key,
            'authorization': env.authorization,
            'api-version': env.api_version,
            'territory': env.territory,
            'device-datetime': args.isoDate,
            'geolocation': `${args.lat};${args.lon}`
        }
    }
    return fetch(Url, options)
            .then(res => res.json())
            .then(res => {
                return res.data
            })
}