import {env} from '../helpers/env';

export const searchlLiveFilm = (args) => {
    console.log('args',args);
     //let Url = `${env.apiUrl}/filmLiveSearch/?query=${args}&n=6`
    let Url = `${env.apiUrl}/film-live-search`

    const options = {
        method: 'GET',
        headers: {
            'client': env.username,
            'x-api-key': env.api_key,
            'authorization': env.authorization,
            'api-version': env.api_version,
            'territory': env.territory,
            'device-datetime': args.isoDate,
        }
    }
    return fetch(Url, options)
            .then(res => res.json())
            .then(res => {
                console.log('live search response', res);
                return res.data
            })
}