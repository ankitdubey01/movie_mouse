import {env} from '../helpers/env';

export const searchfilm = (args) => {
    // let Url = `${env.apiUrl}/filmLiveSearch/?query=star+wars&n=6`
    let Url = `${env.apiUrl}/search-film`

    const options = {
        method: 'GET',
        headers: {
            'client': env.username,
            'x-api-key': env.api_key,
            'authorization': env.authorization,
            'api-version': env.api_version,
            'territory': env.territory,
            'device-datetime': args.isoDate,
        }
    }
    return fetch(Url, options)
            .then(res => res.json())
            .then(res => {
                console.log('show time response', res);
                return res.data
            })
}