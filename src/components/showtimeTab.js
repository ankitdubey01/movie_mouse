import React, { Component } from "react";
import {Text, View, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import {connect} from "react-redux";
import { ListItem, Right, Container, Content, Left} from 'native-base';
import Icons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';

class ShowTimeTab extends Component {
    constructor(props){
        super(props);
        this.state = {
            location: null,
            refreshing: false
        }
    }

    _renderItem = (item) => {
        const {location} = this.props;
        return <ListItem key={item.cinema_id}  >
                    <Left style={{flex: 1, flexDirection: 'column'}} >
                        <Text style={styles.textStyle}>{item.item.cinema_name}</Text>
                        <View style={{flexDirection: 'row',marginTop:10, flexWrap: 'wrap'}}>
                            {item.item.showings.Standard.times.map((time, i)=>(
                                <TouchableOpacity key={i} style={styles.showtimeStyle}>
                                    <Text style={{color:'blue',fontSize:10}}> 
                                        {moment(time.start_time, "hh:mm").format('hh:mm A')}
                                    </Text>
                                </TouchableOpacity>
                                ))}
                        </View>
                        
                    </Left>
    
                    <Right style={{justifyContent: 'center'}} >
                        <Icons name="directions" size={20} color="#7404f4" />
                        <Text style={{fontSize: 10}} >{item.item.distance.toFixed(2)}Km</Text>
                    </Right>
                </ListItem>
    }
    _onRefresh = () => {
        this.setState({refreshing: true})
        const isoDate = new Date().toISOString();
        const args = {
            isoDate
        }
        // this.props.dispatch(actions.CommingSoon(args))

        setTimeout(() => {
            this.setState({refreshing: false});
        }, 2000);
    }
    _keyExtractor = (item, index) => String(item.cinema_id)

    render() {
        // const {film} = this.props;
        console.log(this.props)
        return (
            <Container>
                <Content>
                {
                    this.props.showtime_load && 
                    <View style={styles.loader}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                }
                    {/*<FlatList
                        data={this.props.cinemas ? this.props.cinemas: []}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onRefresh = {this._onRefresh}
                        refreshing = {this.state.refreshing}
                    />*/}
                    <FlatList
                        data={this.props.cinema_search.length > 0 ? this.props.cinema_search: this.props.cinemas}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onRefresh = {this._onRefresh}
                        refreshing = {this.state.refreshing}
                    />
                </Content>
            </Container>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        color: '#000',
        fontSize: 12,
        fontWeight: 'bold'
    },
    imageStyle: {
        height: 150, 
        width: '100%',
        flex: 1,
        resizeMode: 'contain'
    },
    addessStyle : {
        fontSize: 10
    },
    showtimeStyle: {
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:10,
        paddingRight:10,
        marginRight:5,
        marginBottom:5, 
        borderWidth: 1,
        borderColor: '#e0e2e5', 
        borderRadius:3
    }
})


function mapStateToProps(state) {
    return {
        film : state.showtime.film,
        cinemas: state.showtime.cinema,
        showtime_load: state.showtime.load_showtime
    }
}

export default connect(mapStateToProps)(ShowTimeTab)