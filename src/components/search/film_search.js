import React, { Component } from "react";
import {Text, View, StyleSheet, Alert, PermissionsAndroid, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import {connect} from "react-redux";
import {List, ListItem, Right, Container, Content, Left, Header, Button, Item, Icon, Input} from 'native-base';
import Icons from 'react-native-vector-icons/MaterialIcons';
import * as actions from '../../redux/actions';

var self = null;
class FilmSearch extends Component {
    constructor(props){
        super(props);
        self = this;
        this.state = {
            location: null,
            refreshing: false
        }
    }
    componentDidMount(){
        this.props.dispatch(actions.removeSearchLiveFilm())
    }
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: <View style={{flex: 1, flexDirection: 'row'}}>
                            <Item>
                                <Icon name="ios-search" color="#fff" size={20} />
                                <Input 
                                    placeholder="Search films" 
                                    onChangeText={(evt) => self.SearchFilm(evt)} 
                                    style={{fontSize: 13}} 
                                />
                            </Item>
                        </View>,
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 12
            },
        }
        
    }

    SearchFilm = (evt) => {
        //console.log('input value', evt);
        this.props.dispatch(actions.searchLiveFilm(evt))
    }

    _renderItem = (item) => (
        //console.log('item', item)
        item &&
                    <ListItem key={item.item.film_id} onPress={() => {
                                this.props.navigation.navigate('showtime', {
                                    film_id: item.item.film_id
                                })
                        }} >
                        <Left style={{flex: 1, flexDirection: 'column'}} >
                            <Text style={styles.addessStyle}>{item.item.film_name}</Text>
                        </Left>
                    </ListItem>
    )
    _onRefresh = () => {
        this.setState({refreshing: true})
        const isoDate = new Date().toISOString();
        const args = {
            isoDate
        }
        // this.props.dispatch(actions.CommingSoon(args))

        setTimeout(() => {
            this.setState({refreshing: false});
        }, 2000);
    }
    _keyExtractor = (item, index) => String(item.cinema_id)

    render() {
        
        return (
            <Container>
                <Content>
                {/* <Header searchBar rounded>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search" />
                        <Icon name="ios-people" />
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header> */}
                {
                    this.props.cinema.load_cinema && 
                    <View style={styles.loader} >
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                }
                     <FlatList
                        data={this.props.cinema ? this.props.cinema.search_result: []}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onRefresh = {this._onRefresh}
                        refreshing = {this.state.refreshing}
                    />
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        color: '#000',
        fontSize: 12,
        fontWeight: 'bold'
    },
    addessStyle : {
        fontSize: 10
    }
})

function mapStateToProps(state) {
    return {
        cinema : state.liveSearch
    }
}

export default connect(mapStateToProps)(FilmSearch)