import React, { Component } from "react";
import {Text, View, StyleSheet} from 'react-native';
import {connect} from "react-redux";

class DrawerMenu extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <View style={styles.container} >
                <Text style={styles.textStyle} >This is Drawer menu</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        color: 'green'
    }
})

function mapStateToProps(state) {
    return {
        state
    }
}

export default connect(mapStateToProps)(DrawerMenu)