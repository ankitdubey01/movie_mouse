import React, { Component } from "react";
import {Text, View, StyleSheet} from 'react-native';
import {Button, Container} from 'native-base';
import { connect } from "react-redux";
import Mapview, {PROVIDER_GOOGLE} from 'react-native-maps';
import Icons from 'react-native-vector-icons/MaterialIcons';

class MapScreen extends Component {
    constructor(props){
        super(props);
        this.state ={
            latitude: '',
            longitude: '',
            cinema_name: ''
        }
    }
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Direction',
            headerLeft: (
                <Icons 
                    name='dehaze' 
                    color="#fff"
                    size={30} 
                    onPress={()=> {navigation.openDrawer()}} 
                />
            ),
            headerStyle: {
                backgroundColor: '#7404f4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
        
    }

    componentWillMount() {
        const lat = this.props.navigation.getParam('lat', null)
        const long = this.props.navigation.getParam('long', null)
        const cinema_name = this.props.navigation.getParam('name', null)
        this.setState({latitude: lat, longitude: long, cinema_name});
    }
    render() {
        return (
            <Container>
                <Mapview 
                provider={PROVIDER_GOOGLE}
                style={styles.container}
                initialRegion={{
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}
                showsUserLocation={ true }
                >
                {!!this.state.latitude && !!this.state.longitude && 
                    <Mapview.Marker
                        coordinate={{"latitude":this.state.latitude,"longitude":this.state.longitude}}
                        title={this.state.cinema_name}
                    />
                }
                </Mapview>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        color: 'blue'
    }
})

function mapStateToProps(state) {
    return {
        state
    }
}

export default connect(mapStateToProps)(MapScreen)
