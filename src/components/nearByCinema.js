import React, { Component } from "react";
import {Text, View, StyleSheet, Alert, PermissionsAndroid, Button, FlatList, ActivityIndicator, TouchableOpacity} from 'react-native';
import {connect} from "react-redux";
import {List, ListItem, Right, Container, Content, Left, Body} from 'native-base';
import Icons from 'react-native-vector-icons/MaterialIcons';
import * as actions from '../redux/actions';

class NearByCinema extends Component {
    constructor(props){
        super(props);
        this.state = {
            location: null,
            refreshing: false
        }
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Near by cinema',
            headerLeft: (
                <Icons 
                    name='dehaze' 
                    color="#fff"
                    size={20}
                    style={{marginLeft: 10}} 
                    onPress={()=> {navigation.openDrawer()}} 
                />
            ),
            headerStyle: {
                backgroundColor: '#7404f4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 15
            },
        }
        
    }

    requestLocationPermission = async () => {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Movie Mouse Location Permission',
              message:
                'Movie Mouse App needs access to your location.',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use the location');
            await this.findCoordinates()
          } else {
            console.log('Camera permission denied');
            Alert.alert('Location permission debied. Please allow location to access near by cinemas');
          }
        } catch (err) {
          console.warn(err);
        }
      }

    findCoordinates = () => {
        console.log('findlocation called');
        navigator.geolocation.getCurrentPosition(
          position => {
            // const location = JSON.stringify(position);
            console.log('position', position );
            this.setState({ location: position });
            this.locationAction(position);
          },
          error => Alert.alert(error.message),
          { enableHighAccuracy: false, timeout: 5000, maximumAge: 10000 }
        );
    };

    componentWillMount() {
        console.log('will mount')
        this.requestLocationPermission();
    }


    locationAction = (location) => {
        const isoDate = new Date().toISOString();
        const args = {
            lat: location.coords.latitude,
            lon: location.coords.longitude,
            isoDate
        }
        this.props.dispatch(actions.NearbyCinema(args))
    }
    _renderItem = (item) => (
        // console.log('item', item);
        item && <ListItem key={item.cinema_id}>
                    <Left style={{flex: 1, flexDirection: 'column'}}>
                        <Text style={styles.textStyle}>{item.item.cinema_name}</Text>
                        <Text style={styles.addessStyle}>{item.item.address}</Text>
                        <Text style={styles.addessStyle}>{item.item.city}-{item.item.postcode}</Text>
                    </Left>
    
                    <Right style={{justifyContent: 'center'}} >
                    <TouchableOpacity onPress={()=> {
                            this.props.navigation.navigate('map', {
                                lat: item.item.lat,
                                long: item.item.lng,
                                name: item.item.cinema_name
                            });
                        }}>
                        <Icons name="directions" size={20} color="#7404f4" />
                        <Text style={{fontSize: 10}} >{item.item.distance.toFixed(2)}Km</Text>
                        </TouchableOpacity>
                        
                    </Right>
                </ListItem>
    )
    _onRefresh = () => {
        this.setState({refreshing: true})
        const isoDate = new Date().toISOString();
        const args = {
            isoDate
        }
        // this.props.dispatch(actions.CommingSoon(args))

        setTimeout(() => {
            this.setState({refreshing: false});
        }, 2000);
    }
    _keyExtractor = (item, index) => String(item.cinema_id)

    render() {
        
        return (
            <Container>
                <Content>
                {
                    this.props.cinema.load_cinema && 
                    <View style={styles.loader} >
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                }
                    <FlatList
                        data={this.props.cinema ? this.props.cinema.cinema: []}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onRefresh = {this._onRefresh}
                        refreshing = {this.state.refreshing}
                    />
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        color: '#000',
        fontSize: 12,
        fontWeight: 'bold'
    },
    addessStyle : {
        fontSize: 10
    }
})

function mapStateToProps(state) {
    return {
        cinema : state.nearbycinema
    }
}

export default connect(mapStateToProps)(NearByCinema)