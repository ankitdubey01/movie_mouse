import React, { Component } from "react";
import {Text, View, StyleSheet, Alert, PermissionsAndroid, FlatList, ActivityIndicator, Image, ImageBackground,Dimensions, StatusBar} from 'react-native';
import {connect} from "react-redux";
import {Tabs, Tab, Container, Content, Card, ScrollableTab,Input, Item, Icon} from 'native-base';
import Icons from 'react-native-vector-icons/MaterialIcons';
import * as actions from '../redux/actions';
import {ShowTimeTabs} from '../routes';
import ShowTimeTab from './showtimeTab';
import TomorrowTab from './showTimeTabs/tomorrowTab';
import DayAfterTomorrow from './showTimeTabs/dayAfterTomorrow';
import TwoDayAfterTomorrow from './showTimeTabs/twoDayAfterTomorrow';
import Detail from './settings';
import Home from './home';
import moment from 'moment';
import _ from 'lodash';
import Video from 'react-native-video';

class ShowTime extends Component {
    constructor(props){
        super(props);
        this.state = {
            location: null,
            refreshing: false,
            searchResult:[],
            currentTab: 0 
        }
    }
    static navigationOptions = {
        header: null
    }
    cinemasSearch = (event,cinemas) => {
        if(event.length >= 2){
             searchResult = _.filter(cinemas, (cinema) => {
                return cinema.cinema_name.toLowerCase().indexOf(event) > -1
            }); 
            this.setState({searchResult:searchResult});
        }else if(event.length == 0){
            console.log('in else if')
            this.setState({searchResult:[]});
        }
        
    }
    requestLocationPermission = async () => {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Movie Mouse Location Permission',
              message:
                'Movie Mouse App needs access to your location.',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('You can use the location');
            await this.findCoordinates()
          } else {
            console.log('Camera permission denied');
            Alert.alert('Location permission debied. Please allow location to access near by cinemas');
          }
        } catch (err) {
          console.warn(err);
        }
      }

    findCoordinates = () => {
        console.log('findlocation called');
        navigator.geolocation.getCurrentPosition(
          position => {
            console.log('position', position );
            this.setState({ location: position });
            this.locationAction(position);
          },
          error => Alert.alert(error.message),
          { enableHighAccuracy: false, timeout: 5000, maximumAge: 10000 }
        );
    };

    componentWillMount() {
        this.requestLocationPermission();
    }

    getLocalDate = () => {
        const date = new Date().toLocaleDateString();
        const localDate = date.split('/');
        let newFormat = [localDate[2], localDate[1], localDate[0]].join('-');
        const isoDate = new Date().toISOString();
        return {newFormat, isoDate}
    }


    locationAction = (location) => {
        
        const formatedDate = this.getLocalDate();
        const filmId = this.props.navigation.getParam('film_id', null)
        const args = {
            lat: location.coords.latitude,
            lon: location.coords.longitude,
            filmId,
            localdate: formatedDate.newFormat,
            isoDate: formatedDate.isoDate
        }

        const argsTommorow = {
            lat: location.coords.latitude,
            lon: location.coords.longitude,
            filmId,
            localdate: moment().add(1, 'days').format('YYYY-MM-DD'),
            isoDate: formatedDate.isoDate
        }
        const argsDayAfterTommorow = {
            lat: location.coords.latitude,
            lon: location.coords.longitude,
            filmId,
            localdate: moment().add(2, 'days').format('YYYY-MM-DD'),
            isoDate: formatedDate.isoDate
        }
        const args2DayAfterTommorow = {
            lat: location.coords.latitude,
            lon: location.coords.longitude,
            filmId,
            localdate: moment().add(3, 'days').format('YYYY-MM-DD'),
            isoDate: formatedDate.isoDate
        }
        // console.log('formatedDate.newFormat', formatedDate.newFormat,'formatedDate.isoDate',formatedDate.isoDate,'moment',moment().add(1, 'days').format('YYYY-MM-DD'));
        this.props.dispatch(actions.showtime(args))
        this.props.dispatch(actions.showtimeTomorrow(argsTommorow))
        this.props.dispatch(actions.showtimeDayAfterTomorrow(argsDayAfterTommorow))
        this.props.dispatch(actions.showtimeTowDayAfterTomorrow(args2DayAfterTommorow))
        // let song = _.find(this.props.nowShowing_films.films, {filmId});
        //console.log('qwertyuiop',this.props);
    }

    render() {

        const {film,now_showing} = this.props;
        let id = film && film.film_id;
        var selectedMovie = id &&  _.find(now_showing.films, {film_id:id});
        return (
            <Container style={{flex: 1}}>
            <StatusBar hidden={false}/>
            <Content>
                {
                    this.props.showtime_load && 
                    <View style={styles.loader}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                }

                {
                    this.props.film && <Card>
                        {
                            selectedMovie.film_trailer ? 
                                <Video source={{uri: `${selectedMovie.film_trailer}`}}   // Can be a URL or a local file.
                                    ref={(ref) => {
                                    this.player = ref
                                    }}                                      // Store reference
                                    onBuffer={this.onBuffer}                // Callback when remote video is buffering
                                    onError={this.videoError}               // Callback when video cannot be loaded
                                    style={styles.backgroundVideo}
                                    controls={true}
                                    resizeMode={"stretch"}
                                    style={{ width: '100%', height: 200 }}
                                    repeat={false}
                                    paused={true}
                                    fullscreen={true}
                                    filterEnabled={true}
                                />:
                            film.images ? 
                            <ImageBackground source={{ uri: film.images.poster[Object.keys(film.images.poster)[0]].medium.film_image}} blurRadius={1} style={{height: 100, width: '100%', resizeMode: 'stretch'}} >
                                <Image 
                                    source={{ uri: film.images.poster[Object.keys(film.images.poster)[0]].medium.film_image}}
                                    style={styles.imageStyle} 
                                />
                            </ImageBackground>
                            :<Image 
                                source={require('../assets/dummy.png')}
                                style={styles.imageStyle} 
                            />
                        }
                        <View style={{marginTop: 5, marginBottom: 5}} >
                            
                                <Text style={styles.textStyle} >{this.props.film.film_name}</Text>
                            
                        </View>
                    </Card>
                }
                {/* <ShowTimeTabs /> */}

                <View style={{flex: 1}} hastabs>
                
                    <Tabs 
                    tabBarUnderlineStyle={{backgroundColor:'#7404f4', height:1}} 
                    renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#fff", borderWidth: 0, marginBottom: 10, height: 40 }} /> }
                    >
                        <Tab heading="Showtimes"
                        tabStyle={{backgroundColor: '#fff'}}
                        textStyle={{color: '#000'}}
                        activeTabStyle={{backgroundColor: '#fff'}}
                        activeTextStyle={{color: '#000'}}
                        >
                        <Item regular style={{marginLeft: 10, marginRight: 10, height: 30, borderRadius:5}}>
                            <Icon name="ios-search" color="#fff" style={{fontSize: 15}} />
                            <Input 
                                placeholder="Search cinemas" 
                                onChangeText={(evt) => this.cinemasSearch(evt,this.props.cinemas)} 
                                style={{fontSize: 10 }} 
                            />
                        </Item>
                
                        <Tabs  
                            tabBarUnderlineStyle={{backgroundColor: 'transparent'}}
                            renderTabBar={()=> <ScrollableTab style={{ backgroundColor: "#fff", borderWidth: 0, height: 30, marginTop: 10 }} /> }
                        >
                            <Tab heading={moment().format('ddd DD')}
                                tabStyle={styles.tabStyle} 
                                textStyle={styles.textStyle} 
                                activeTabStyle={styles.activeTabStyle} 
                                activeTextStyle={styles.activeTextStyle}
                            >
                                <ShowTimeTab 
                                navigation={this.props.navigation}
                                cinema_search ={this.state.searchResult}
                            />
                            </Tab>
                            
                            <Tab heading={moment().add(1, 'days').format('ddd DD')} 
                                tabStyle={styles.tabStyle} 
                                textStyle={styles.textStyle} 
                                activeTabStyle={styles.activeTabStyle} 
                                activeTextStyle={styles.activeTextStyle}
                            >
                                <TomorrowTab
                                navigation={this.props.navigation}
                                cinema_search ={this.state.searchResult}
                                />
                            </Tab>
                            <Tab heading={moment().add(2, 'days').format('ddd DD')} 
                                tabStyle={styles.tabStyle} 
                                textStyle={styles.textStyle} 
                                activeTabStyle={styles.activeTabStyle} 
                                activeTextStyle={styles.activeTextStyle}
                            >
                                <DayAfterTomorrow 
                                navigation={this.props.navigation}
                                cinema_search ={this.state.searchResult}
                                />
                            </Tab>
                            <Tab heading={moment().add(3, 'days').format('ddd DD')}
                                tabStyle={styles.tabStyle} 
                                textStyle={styles.textStyle} 
                                activeTabStyle={styles.activeTabStyle} 
                                activeTextStyle={styles.activeTextStyle}
                            >
                                <TwoDayAfterTomorrow 
                                navigation={this.props.navigation}
                                cinema_search ={this.state.searchResult}
                                />
                            </Tab>
                        </Tabs>
                         
                        </Tab>
                        <Tab heading="More"
                            tabStyle={{backgroundColor: '#fff'}}
                            textStyle={{color: '#000'}}
                            activeTabStyle={{backgroundColor: '#fff'}}
                            activeTextStyle={{color: '#000'}}
                        >
                            <Detail synopsis={selectedMovie}/>
                        </Tab>
                    </Tabs>
                </View> 
                </Content>        
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    
    imageStyle: {
        height: 150, 
        width: '100%',
        flex: 1,
        resizeMode: 'center'
    },
    addessStyle : {
        fontSize: 10
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },

    tabStyle:{
        backgroundColor: '#edf0f4',
        paddingLeft: 5,
        paddingRight: 5,
        borderRadius: 20
    },
    activeTabStyle:{
        backgroundColor: '#7404f4',
        paddingLeft: 5,
        paddingRight: 5,
        borderRadius: 20
    },
    textStyle:{
        color: '#7404f4',
        fontSize: 12
    }, 
    activeTextStyle:{
        color: '#fff'
    }
})

function mapStateToProps(state) {
    return {
        film : state.showtime.film,
        cinemas: state.showtime.cinema,
        showtime_load: state.showtime.load_showtime,
        now_showing : state.nowShowing_films,
    }
}

export default connect(mapStateToProps)(ShowTime)