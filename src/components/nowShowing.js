import React, { Component } from "react";
import {Text, View, StyleSheet, ActivityIndicator, Image, FlatList, ImageBackground, TouchableHighlight, TouchableOpacity,  Alert} from 'react-native';
import {connect} from "react-redux";
import Icons from 'react-native-vector-icons/MaterialIcons';
import * as actions from '../redux/actions';
import {Container, Content, Card, Button, Header, Item, Icon, Input} from 'native-base';
import moment from 'moment';
import Modal from 'react-native-modal';
import FIcons from 'react-native-vector-icons/FontAwesome';
import Poster from '../containers/poster';
import {Contentloader} from '../containers/contentLoader';

var self = null;
class NowShowing extends Component {
    constructor(props){
        super(props);
        self = this;
        this.state = {
            device_date: '',
            refreshing: false,
            modalVisible: false
        }
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Now showing movies',
            headerLeft: (
                <Icons 
                    name='dehaze' 
                    color="#fff"
                    size={20}
                    style={{marginLeft: 10}} 
                    onPress={()=> {navigation.openDrawer()}} 
                />
            ),
            headerRight: (
                <Icons 
                    name='search' 
                    color="#fff"
                    size={20}
                    style={{marginRight: 10}} 
                    onPress={()=> {navigation.navigate('filmSearch')}} 
                />
            ),
            headerStyle: {
                backgroundColor: '#7404f4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 15,
                fontFamily: 'sans-serif-medium'
            },
        }
        
      }

    componentDidMount() {
        const isoDate = new Date().toISOString();
        const args = {
            isoDate
        }
        this.props.dispatch(actions.nowShowing(args))
    }
    setModalVisible = (visible) => {
        this.setState({modalVisible: visible});
    }
    _renderItem = (data) => {
            const item = data.item;
            return <Poster item={item} navigation={this.props.navigation} />
    }
    _onRefresh = () => {
        this.setState({refreshing: true})
        const isoDate = new Date().toISOString();
        const args = {
            isoDate
        }
        this.props.dispatch(actions.nowShowing(args))

        setTimeout(() => {
            this.setState({refreshing: false});
        }, 2000);
    }
    _keyExtractor = (item, index) => String(item.film_id)
    
    render() {
        
        return (
            <Container style={styles.container}>
                <Content>
                    {
                        this.props.nowShowingfilms.load_now_showing_movies && 
                        // <Contentloader />
                        <View style={styles.loader} >
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    }
                    <FlatList
                        data={this.props.nowShowingfilms ? this.props.nowShowingfilms.films: []}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onRefresh = {this._onRefresh}
                        refreshing = {this.state.refreshing}
                    />
                    
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    loader: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    // modalView: {
    //     marginTop: 20,
    //     borderRadius: 5,
    //     backgroundColor: 'gray'
    // }
})

function mapStateToProps(state) {
    return {
        nowShowingfilms: state.nowShowing_films
    }
}

export default connect(mapStateToProps)(NowShowing)