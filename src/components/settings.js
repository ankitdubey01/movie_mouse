import React, { Component } from "react";
import {Text, View, StyleSheet} from 'react-native';
import { connect } from "react-redux";
import Video from 'react-native-video';
class Setting extends Component {
    constructor(props){
        super(props);
    }
    render() {
        console.log('Setting',this.props);
        return (
            <View style={styles.container} >
            {/*<Text style={styles.h3}>Trailer</Text>
                <Video source={{uri: "https://trailer.movieglu.com/263100_high_V2.mp4"}}   // Can be a URL or a local file.
                    ref={(ref) => {
                    this.player = ref
                    }}                                      // Store reference
                    onBuffer={this.onBuffer}                // Callback when remote video is buffering
                    onError={this.videoError}               // Callback when video cannot be loaded
                    style={styles.backgroundVideo}
                    controls={true}
                    resizeMode={"stretch"}
                    style={{ width: '100%', height: '50%' }}
                    repeat={false}
                    paused={true}
                    fullscreen={true}
                />*/}
                <Text style={styles.h3}>Synopsis</Text>
                {this.props.synopsis && <Text style={styles.textStyle}>{this.props.synopsis.synopsis_long}</Text>}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        color: '#cecaca',
        fontSize: 12,
        padding:10
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },
      h3:{
          fontSize:12,
          padding:10,
          color:'#000',
          fontWeight:'bold'
        }
})
function mapStateToProps(state) {
    return {
        nowShowing:state.nowShowing_films
    }
}

export default connect(mapStateToProps)(Setting)