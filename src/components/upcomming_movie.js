import React, { Component } from "react";
import {Text, View, StyleSheet, Image, FlatList, ActivityIndicator, TouchableOpacity, Modal, ImageBackground} from 'react-native';
import {connect} from "react-redux";
import * as actions from '../redux/actions';
import {Container, Content, Card, CardItem, Thumbnail, Button, Left, Body, Icon} from 'native-base';
import moment from 'moment';
import DummyImage from '../assets/dummy.png';
import Icons from 'react-native-vector-icons/MaterialIcons';

class Upcomming extends Component {
    constructor(props){
        super(props);
        this.state = {
            device_date: '',
            refreshing: false,
            modalVisible: false
        }
    }
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Upcomming movies',
            headerLeft: (
                <Icons 
                    name='dehaze' 
                    color="#fff"
                    size={20}
                    style={{marginLeft: 10}} 
                    onPress={()=> {navigation.openDrawer()}} 
                />
            ),
            headerStyle: {
                backgroundColor: '#7404f4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'normal',
                fontSize: 15
            },
        }
        
      }

    componentDidMount() {
        const isoDate = new Date().toISOString();
        const args = {
            isoDate
        }
        this.props.dispatch(actions.CommingSoon(args));
    }
    setModalVisible = (visible) => {
        this.setState({modalVisible: visible});
    }
    _renderItem = (data) => {
            const item = data.item;
            return <TouchableOpacity 
                onPress={() => {
                this.setModalVisible(true);
            }}>
            <Card>
                <View style={styles.CardView}>
                        
                        {
                            item.images.poster[Object.keys(item.images.poster)[0]] ?
                            <ImageBackground source={{ uri: item.images.poster[Object.keys(item.images.poster)[0]].medium.film_image}} blurRadius={1} style={{height: 210, width: '100%', resizeMode: 'cover'}} >
                                <Image 
                                    source={{ uri: item.images.poster[Object.keys(item.images.poster)[0]].medium.film_image}}
                                    style={styles.imageStyle} 
                                />
                            </ImageBackground>
                            :<Image 
                                source={require('../assets/dummy.png')}
                                style={styles.imageStyle} 
                            />
                        }
                        
                    
                </View>
                <View style={styles.CardView}>
                    <Text style={styles.discription}>
                        {item.synopsis_long.substr(0,200)+'...'}
                    </Text>
                </View>
                <View style={{marginTop: 5, marginBottom: 5}} >
                    <View style={[styles.CardView, styles.horizontal]} >
                        <Text style={styles.headingText} >Name : </Text>
                        <Text style={styles.textStyle} >{item.film_name}</Text>
                    </View>
                    <View style={[styles.CardView, styles.horizontal]} >
                        <Text style={styles.headingText} >Release date: </Text>
                        <Text style={styles.textStyle} >{moment(item.release_dates[0].release_date).format("MMMM D, YYYY")}({item.release_dates[0].notes})</Text>

                        <View>
                            <Text style={{color: '#f22ea0', fontSize: 10}}>Rating : {item.age_rating[0].rating}</Text>
                        </View> 
                    </View>
                </View>
                
            </Card>
            </TouchableOpacity>
    }
    _onRefresh = () => {
        this.setState({refreshing: true})
        const isoDate = new Date().toISOString();
        const args = {
            isoDate
        }
        this.props.dispatch(actions.CommingSoon(args))

        setTimeout(() => {
            this.setState({refreshing: false});
        }, 2000);
    }
    _keyExtractor = (item, index) => String(item.film_id)

    render() {
        
        return (
            <Container style={styles.container} >
                <Content>
                    {
                        this.props.comming_soon.load_films && 
                        <View style={styles.loader} >
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    }
                    <FlatList
                        data={this.props.comming_soon ? this.props.comming_soon.films: []}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                        onRefresh = {this._onRefresh}
                        refreshing = {this.state.refreshing}
                    />

                    <View>
                        <Modal
                            animationType="fade"
                            transparent={false}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                this.setModalVisible(!this.state.modalVisible);
                            }}>
                            <View style={{marginTop: 22}}>
                                <View>
                                <Text>This is Modal!</Text>

                                <Button style={{position:'absolute', top: 5, right: 10}} rounded danger
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                                    <Icon name="close" />
                                </Button>
                                </View>
                            </View>
                        </Modal>
                    </View>
                    
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    discription: {
        textAlign: 'left',
        marginTop: 5,
        fontSize: 10
    },
    textStyle: {
        fontWeight: 'normal',
        flex: 1,
        fontSize: 10
    },
    headingText: {
        color: "blue",
        fontWeight: 'bold',
        fontSize: 10
    },
    loader: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    CardView: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 5
    },
    imageStyle: {
        height: 210, 
        width: '100%',
        flex: 1,
        resizeMode: 'contain'
    },
    horizontal: {
        flexDirection: 'row',
        alignItems: 'center',
      }
})


function mapStateToProps(state) {
    return {
        comming_soon: state.comming_soon
    }
}

export default connect(mapStateToProps)(Upcomming)