import React, { Component } from "react";
import {Text, View, StyleSheet} from 'react-native';
import {Button} from 'native-base';
import { connect } from "react-redux";

class HomeScreen extends Component {
    
    
    render() {
        console.log('home render');
        return (
            <View style={styles.container} >
                <Text styles={styles.textStyle} >This is home screen</Text>
                {/* <Button primary onPress={() => this.props.navigation.navigate('setting')} >
                    <Text>go to setting</Text>
                </Button> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textStyle: {
        color: 'blue'
    }
})

function mapStateToProps(state) {
    return {
        state
    }
}

export default connect(mapStateToProps)(HomeScreen)
