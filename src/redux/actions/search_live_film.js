import * as constants from '../constants';
import * as services from '../../services';

export const searchLiveFilm = (args) => {
    return dispatch => {
        dispatch({
            type: constants.SEARCH_LIVE_FILM
        })
        services.searchlLiveFilm(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.SEARCH_LIVE_FILM_SUCCESS,
                    payload: response.films
                })
            }else{
                dispatch({
                    type: constants.SEARCH_LIVE_FILM_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.SEARCH_LIVE_FILM_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}

export const removeSearchLiveFilm = () => {
    return dispatch => {
        dispatch({
            type: constants.REMOVE_SEARCH_LIVE_FILM
        })
        // services.searchlLiveFilm(args)
        // .then(response =>{
        //     if(response.status.state === 'OK'){
        //         dispatch({
        //             type: constants.SEARCH_LIVE_FILM_SUCCESS,
        //             payload: response.films
        //         })
        //     }else{
        //         dispatch({
        //             type: constants.SEARCH_LIVE_FILM_FAILURE
        //         })
        //     }
        // })
        // .catch(error =>{
        //     dispatch({
        //         type: constants.SEARCH_LIVE_FILM_FAILURE,
        //     })
        //     alert('Oops! something went wrong');
        // })
    }
}

