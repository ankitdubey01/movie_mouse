import * as constants from '../constants';
import * as services from '../../services';

export const CommingSoon = (args) => {
    return dispatch => {
        dispatch({
            type: constants.UPCOMMING_MOVIES_INITIATE
        })
        services.CommingSoon(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.UPCOMMING_MOVIES_SUCCESS,
                    payload: response.films
                })
            }else{
                dispatch({
                    type: constants.UPCOMMING_MOVIES_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.UPCOMMING_MOVIES_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}