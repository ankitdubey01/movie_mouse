import * as constants from '../constants';
import * as services from '../../services';

export const showtime = (args) => {
    return dispatch => {
        dispatch({
            type: constants.SHOWTIME_INITIATE
        })
        services.showtime(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.SHOWTIME_SUCCESS,
                    payload: response
                })
            }else{
                dispatch({
                    type: constants.SHOWTIME_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.SHOWTIME_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}

export const showtimeTomorrow = (args) => {
    return dispatch => {
        dispatch({
            type: constants.SHOWTIME_TOMORROW_INITIATE
        })
        services.showtime(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.SHOWTIME_TOMORROW_SUCCESS,
                    payload: response
                })
            }else{
                dispatch({
                    type: constants.SHOWTIME_TOMORROW_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.SHOWTIME_TOMORROW_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}

export const showtimeDayAfterTomorrow = (args) => {
    return dispatch => {
        dispatch({
            type: constants.SHOWTIME_DAYAFTERTOMORROW_INITIATE
        })
        services.showtime(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.SHOWTIME_DAYAFTERTOMORROW_SUCCESS,
                    payload: response
                })
            }else{
                dispatch({
                    type: constants.SHOWTIME_DAYAFTERTOMORROW_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.SHOWTIME_DAYAFTERTOMORROW_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}

export const showtimeTowDayAfterTomorrow = (args) => {
    return dispatch => {
        dispatch({
            type: constants.SHOWTIME_TWODAYAFTERTOMORROW_INITIATE
        })
        services.showtime(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.SHOWTIME_TWODAYAFTERTOMORROW_SUCCESS,
                    payload: response
                })
            }else{
                dispatch({
                    type: constants.SHOWTIME_TWODAYAFTERTOMORROW_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.SHOWTIME_TWODAYAFTERTOMORROW_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}

