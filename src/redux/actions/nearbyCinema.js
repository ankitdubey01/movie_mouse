import * as constants from '../constants';
import * as services from '../../services';

export const NearbyCinema = (args) => {
    return dispatch => {
        dispatch({
            type: constants.NEARBY_CINEMA_INITIATE
        })
        services.nearbyCinema(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.NEARBY_CINEMA_SUCCESS,
                    payload: response.cinemas
                })
            }else{
                dispatch({
                    type: constants.NEARBY_CINEMA_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.NEARBY_CINEMA_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}