import * as constants from '../constants';
import * as services from '../../services';

export const nowShowing = (args) => {
    return dispatch => {
        dispatch({
            type: constants.NOW_SHOWING_INITIATE
        })
        services.nowShowing(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.NOW_SHOWING_SUCCESS,
                    payload: response.films
                })
            }else{
                dispatch({
                    type: constants.NOW_SHOWING_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.NOW_SHOWING_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}