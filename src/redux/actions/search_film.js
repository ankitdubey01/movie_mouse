import * as constants from '../constants';
import * as services from '../../services';

export const searchfilm = (args) => {
    return dispatch => {
        dispatch({
            type: constants.SEARCH_FILM_INITIATE
        })
        services.searchfilm(args)
        .then(response =>{
            if(response.status.state === 'OK'){
                dispatch({
                    type: constants.SEARCH_FILM_SUCCESS,
                    payload: response.films
                })
            }else{
                dispatch({
                    type: constants.SEARCH_FILM_FAILURE
                })
            }
        })
        .catch(error =>{
            dispatch({
                type: constants.SEARCH_FILM_FAILURE,
            })
            alert('Oops! something went wrong');
        })
    }
}