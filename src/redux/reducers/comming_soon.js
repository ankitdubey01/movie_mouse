import * as constants from '../constants';
const initialValues = {
    load_films: false
}

export const commingSoon = (state = initialValues, action) => {
    switch (action.type) {
        case constants.UPCOMMING_MOVIES_INITIATE:
            state.load_films = true
            return {
                ...state
            }
        
        case constants.UPCOMMING_MOVIES_SUCCESS:
            state.load_films = false
            return {
                films: action.payload,
                ...state
            }
        case constants.UPCOMMING_MOVIES_FAILURE:
            state.load_films = false
            return {
                films: [],
                ...state
            }
        default:
            return {
                ...state
            }
    }
}