import * as constants from '../constants';
const initialValues = {
    load_cinema: false
}

export const nearbycinema = (state = initialValues, action) => {
    switch (action.type) {
        case constants.NEARBY_CINEMA_INITIATE:
            state.load_cinema = true
            return {
                ...state
            }
        
        case constants.NEARBY_CINEMA_SUCCESS:
            state.load_cinema = false
            return {
                cinema: action.payload,
                ...state
            }
        case constants.NEARBY_CINEMA_FAILURE:
            state.load_cinema = false
            return {
                cinema: [],
                ...state
            }
        default:
            return {
                ...state
            }
    }
}