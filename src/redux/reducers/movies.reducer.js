import * as constants from '../constants';
const initialValues = {
    load_now_showing_movies: false
}

export const nowShowing = (state = initialValues, action) => {
    switch (action.type) {
        case constants.NOW_SHOWING_INITIATE:
            state.load_now_showing_movies = true
            return {
                ...state
            }
        
        case constants.NOW_SHOWING_SUCCESS:
            state.load_now_showing_movies = false
            return {
                films: action.payload,
                ...state
            }
        case constants.NOW_SHOWING_FAILURE:
            state.load_now_showing_movies = false
            return {
                nowShowingMovies: [],
                ...state
            }
        default:
            return {
                ...state
            }
    }
}