import * as constants from '../constants';
const initialValues = {
    load_showtime: false
}

export const showtime = (state = initialValues, action) => {
    switch (action.type) {
        case constants.SHOWTIME_INITIATE:
            state.load_showtime = true
            return {
                ...state
            }
        
        case constants.SHOWTIME_SUCCESS:
            state.load_showtime = false
            return {
                cinema: action.payload.cinemas,
                film: action.payload.film,
                ...state
            }
        case constants.SHOWTIME_FAILURE:
            state.load_showtime = false
            return {
                cinema: [],
                film: [],
                ...state
            }
        default:
            return {
                ...state
            }
    }
}