import * as constants from '../constants';
const initialValues = {
    load_showtime: false
}

export const tomorrow = (state = initialValues, action) => {
    switch (action.type) {
        case constants.SHOWTIME_TOMORROW_INITIATE:
            state.load_showtime = true
            return {
                ...state
            }
        
        case constants.SHOWTIME_TOMORROW_SUCCESS:
            state.load_showtime = false
            return {
                cinema: action.payload.cinemas,
                film: action.payload.film,
                ...state
            }
        case constants.SHOWTIME_TOMORROW_FAILURE:
            state.load_showtime = false
            return {
                cinema: [],
                film: [],
                ...state
            }
        default:
            return {
                ...state
            }
    }
}


export const dayAfterTomorrow = (state = initialValues, action) => {
    switch (action.type) {
        case constants.SHOWTIME_DAYAFTERTOMORROW_INITIATE:
            state.load_showtime = true
            return {
                ...state
            }
        
        case constants.SHOWTIME_DAYAFTERTOMORROW_SUCCESS:
            state.load_showtime = false
            return {
                cinema: action.payload.cinemas,
                film: action.payload.film,
                ...state
            }
        case constants.SHOWTIME_DAYAFTERTOMORROW_FAILURE:
            state.load_showtime = false
            return {
                cinema: [],
                film: [],
                ...state
            }
        default:
            return {
                ...state
            }
    }
}

export const towDayAfterTomorrow = (state = initialValues, action) => {
    switch (action.type) {
        case constants.SHOWTIME_TWODAYAFTERTOMORROW_INITIATE:
            state.load_showtime = true
            return {
                ...state
            }
        
        case constants.SHOWTIME_TWODAYAFTERTOMORROW_SUCCESS:
            state.load_showtime = false
            return {
                cinema: action.payload.cinemas,
                film: action.payload.film,
                ...state
            }
        case constants.SHOWTIME_TWODAYAFTERTOMORROW_FAILURE:
            state.load_showtime = false
            return {
                cinema: [],
                film: [],
                ...state
            }
        default:
            return {
                ...state
            }
    }
}