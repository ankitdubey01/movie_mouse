import {combineReducers} from 'redux';
import {nowShowing} from './movies.reducer';
import {commingSoon} from './comming_soon'
import {nearbycinema} from './nearbyCinema';
import {showtime} from './showtime';
import {liveSearch} from './live_search_film';
import {tomorrow} from './nextShowTime';
import {dayAfterTomorrow} from './nextShowTime';
import {towDayAfterTomorrow} from './nextShowTime';

export default combineReducers({
    nowShowing_films : nowShowing,
    comming_soon: commingSoon,
    nearbycinema: nearbycinema,
    showtime: showtime,
    liveSearch:liveSearch,
    tomorrow:tomorrow,
    dayAfterTomorrow:dayAfterTomorrow,
    towDayAfterTomorrow:towDayAfterTomorrow,
})