import * as constants from '../constants';
const initialValues = {
    live_search: false
}

export const liveSearch = (state = initialValues, action) => {
    switch (action.type) {
        case constants.SEARCH_LIVE_FILM:
            state.live_search = true
            return {
                ...state
            }
        
        case constants.SEARCH_LIVE_FILM_SUCCESS:
            state.load_films = false
            return {
                search_result: action.payload,
                ...state
            }
        case constants.SEARCH_LIVE_FILM_FAILURE:
            state.load_films = false
            return {
                search_result: [],
                ...state
            }
        case constants.REMOVE_SEARCH_LIVE_FILM :
        state.live_search =    false
            return {
                
            }
        default:
            return {
                ...state
            }
    }
}