import React, {component} from 'react';
import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import {createLogger} from 'redux-logger';
const loggerMiddleware = createLogger();
const middleware = applyMiddleware(thunk,loggerMiddleware);

const store = createStore(rootReducer, middleware);
export default store;