import React, { Component } from "react";
import {Text, View} from 'react-native';
import {createAppContainer, createBottomTabNavigator, createDrawerNavigator,  createStackNavigator, createMaterialTopTabNavigator} from 'react-navigation';
import NowShowing from './components/nowShowing';
import Upcomming from './components/upcomming_movie'
import HomeScreen from './components/home';
import FilmShowTime from './components/filmShowTime';
import NearByCinema from './components/nearByCinema';
import Sidebar from './components/sidebar';
import Drawer from './components/drawerMenu';
import ShowtimeTab from './components/showtimeTab';
import Icons from 'react-native-vector-icons/MaterialIcons';
import DetailScreen from './components/settings';
import MapScreen from './components/nowShowing/googleMap';
import FilmSearch from './components/search/film_search';

const HeaderIcon = () => (
        <Icons 
            name='dehaze' 
            color="#fff"
            size={30} 
            onPress={()=> alert('Drawer comming soon')} 
        />
)


const nowShowingStack = createStackNavigator({
    nowShowing: {
        screen : NowShowing
    },
    showtime: {
        screen : FilmShowTime
    },
    map: {
        screen: MapScreen
    },
    filmSearch: {
        screen: FilmSearch
    }
},
{
    initialRouteName: 'nowShowing',
    statusbar: true
})

const UpcommingStack = createStackNavigator({
    Upcomming: {
        screen : Upcomming
    },
    Details: {
        screen : DetailScreen,
        navigationOptions : {
            title: 'Movie information',
            headerLeft: (
                <HeaderIcon />
            ),
            headerStyle: {
                backgroundColor: '#7404f4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    }
})

const nearByCinemaStack = createStackNavigator({
    nearByCinema: {
        screen : NearByCinema
    },
    map: {
        screen: MapScreen
    },
    Details: {
        screen : DetailScreen,
        navigationOptions : {
            title: 'cinema hall information',
            headerLeft: (
                <HeaderIcon />
            ),
            headerStyle: {
                backgroundColor: '#7404f4',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    }
})


const Tabs = createAppContainer(createBottomTabNavigator(
    {
        'Now showing': nowShowingStack,
        'Upcoming': UpcommingStack,
        'Near by cinema': nearByCinemaStack
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let IconComponent = Icons;
                let iconName;
                if (routeName === 'Now showing') {
                    iconName = 'videocam'; 
                } else if (routeName === 'Upcoming') {
                    iconName = 'movie';
                }else if(routeName === 'Near by cinema'){
                    iconName = 'location-on'
                }
    
                return <IconComponent name={iconName} size={17} color={tintColor} />;
            },
        }),
        tabBarOptions: {
          activeTintColor: '#7404f4',
          inactiveTintColor: '#ccc9ca',
          labelStyle: {
              fontSize: 9
          },
          tabStyle: {
              backgroundColor: 'transparent',
              paddingVertical: 2
          }
        },
      }
))

export default createAppContainer(createDrawerNavigator(
    {
        Home: {
            screen: Tabs
        },
       
    },
    {
        drawerPosition: 'left',
        initialRouteName: 'Home',
        contentComponent: props=> <Sidebar {...props} />,
        drawerBackgroundColor: '#e3e8ef',
        drawerWidth: 300,
        gesturesEnabled: true
    }
));